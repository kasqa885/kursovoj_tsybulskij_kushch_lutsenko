﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bin_packing
{
    public class Project
    {
        private Guid _id;
        private string _name;
        private ObservableCollection<Plate> _plates = new ObservableCollection<Plate>();

        

        public Guid id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value;  }
        }

        public ObservableCollection<Plate> plates
        {
            get { return _plates; }
            set { _plates = value; }
        }

        public Project(string name)
        {
            this.id = Guid.NewGuid();
            this.name = name;
        }

        public Project()
        {
            DateTime date = new DateTime();
            this.id = Guid.NewGuid();
            this.name = "MyProject_" + date;
        }
        public Project(Project project)
        {
            id = project.id;
            name = project.name;
            plates = project.plates;
        }

        public Plate addPlate(Plate plate)
        {
            _plates.Add(plate);
            //return _plates.Where(p => p.id == plate.id);
            return plate;
        }

        public void removePlate(Plate plate)
        {
            _plates.Remove(plate);
        }
    }
}
