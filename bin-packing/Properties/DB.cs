﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SQLite;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Data;


namespace bin_packing.Properties
{
    class DB
    {
        public string path = "";
        public DB(string p) {
            path = p;
        }
        public void db_conection()
        {

            try
            {
                if (!File.Exists(@path))
                {
                    SQLiteConnection.CreateFile(@path);
                }

                using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;")) // в строке указывается к какой базе подключаемся
                {
                    // строка запроса, который надо будет выполнить
                    string commandText = "CREATE TABLE IF NOT EXISTS [Plates] ( [id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [plate_name] VARCHAR(50), [height] INTEGER(10), " +
                        "[width] INTEGER(10));" +
                        "CREATE TABLE IF NOT EXISTS [Parts] ( [id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [part_name] VARCHAR(50), [height] INTEGER(10), " +
                        "[width] INTEGER(10));";
                    SQLiteCommand Command = new SQLiteCommand(commandText, Connect);
                    Connect.Open(); // открыть соединение
                    Command.ExecuteNonQuery(); // выполнить запрос
                    Connect.Close(); // закрыть соединение
                }
            }
            catch {
                MessageBox.Show("Ups! Something was going wrong!");
            }

        }
        public DataSet get_all_rows_plates()
        {
            string sql = "SELECT * FROM Plates";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                // Создаем объект DataAdapter
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sql, Connect);
                // Создаем объект Dataset
                DataSet ds = new DataSet();
                // Заполняем Dataset
                adapter.Fill(ds);
                Connect.Close();
                // Отображаем данные   
                return ds;
            }

        }
        public string get_row_plates_by_id(int id)
        {
            string sql = "SELECT * FROM Plates Where id ==" + id;
            string row = "";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                SQLiteCommand command = new SQLiteCommand(sql, Connect);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read()) {
                    row = reader["id"] + ";" + reader["plate_name"] + ";" + reader["height"] + ";" + reader["width"];
                }
                Connect.Close();
            }
            return row;
        }
        public void post_row_plates(string name, int h, int w) {

            string sql = "INSERT INTO Plates (plate_name,height,width) VALUES ('" + name+"',"+h+","+w+")";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;")) {
                Connect.Open();
                SQLiteCommand command = new SQLiteCommand(sql, Connect);
                command.ExecuteNonQuery();
                Connect.Close();
            }
                
        }
        public void delete_row_plates_by_id(int id)
        {
            string sql = "DELETE * FROM Plates Where id ==" + id + " LIMIT 1";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                SQLiteCommand command = new SQLiteCommand(sql, Connect);
                command.ExecuteNonQuery();
                Connect.Close();
            }
            
        }
        public DataSet get_all_rows_parts()
        {
            string sql = "SELECT * FROM Parts";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                // Создаем объект DataAdapter
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sql, Connect);
                // Создаем объект Dataset
                DataSet ds = new DataSet();
                // Заполняем Dataset
                adapter.Fill(ds);
                Connect.Close();
                // Отображаем данные   
                return ds;
            }

        }
        public string get_row_parts_by_id(int id)
        {
            string sql = "SELECT * FROM Parts Where id ==" + id;
            string row = "";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                SQLiteCommand command = new SQLiteCommand(sql, Connect);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    row = reader["id"] + ";" + reader["part_name"] + ";" + reader["height"] + ";" + reader["width"];
                }
                Connect.Close();
            }
            return row;
        }
        public void post_row_part(string name, int h, int w)
        {

            string sql = "INSERT INTO Plates (part_name,height,width) VALUES ('" + name + "'," + h + "," + w + ")";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                SQLiteCommand command = new SQLiteCommand(sql, Connect);
                command.ExecuteNonQuery();
                Connect.Close();
            }

        }
        public void delete_row_parts_by_id(int id)
        {
            string sql = "DELETE * FROM Parts Where id ==" + id + " LIMIT 1";
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=" + path + "; Version=3;"))
            {
                Connect.Open();
                SQLiteCommand command = new SQLiteCommand(sql, Connect);
                command.ExecuteNonQuery();
                Connect.Close();
            }

        }
    }    

}
