﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bin_packing
{
    class FreeRectangle : Rectangle
    {
        public FreeRectangle(int width, int height, int x, int y)
        {
            this.width = width;
            this.height = height;
            this.x = x;
            this.y = y;
        }

        public int Area()
        {
            return width * height;
        }
    }
}
