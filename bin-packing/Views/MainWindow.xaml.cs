﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Globalization;

using System.Windows.Threading;

using bin_packing.Views;
using bin_packing.Properties;



namespace bin_packing.Views
{
    public partial class MainWindow : Window
    {
        Random r = new Random();

        //??
        Project test;

        //public ObservableCollection<Tile> Tiles { get; set; }
         public MainWindow(Project project)
        {
            test = new Project(project);
            InitializeComponent();

            DB db = new DB(AppDomain.CurrentDomain.BaseDirectory + @"\DataBase.db");

            db.db_conection();
            TilesList.ItemsSource = test.plates;
            if(test.plates.Any())
            { 
                render(test.plates[0]);
            }
        }
        public MainWindow(string pr_name)
        {
            test = new Project(pr_name);
            InitializeComponent();

            this.Title = pr_name;

            DB db = new DB(AppDomain.CurrentDomain.BaseDirectory + @"\DataBase.db");

            db.db_conection();

            TilesList.ItemsSource = test.plates;

            if (test.plates.Any())
            {
                render(test.plates[0]);
            }
        }

        private void saveProjectAsJson(object sender, RoutedEventArgs e) {
            JsonController js_con = new JsonController();
            js_con.json_writer(js_con.json_convertor_to(test), test.name);
            MessageBox.Show("Проект успішно збережено", String.Empty, MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private void Reset(object sender, RoutedEventArgs e)
        {
            //EditFragPanel.Visibility = Visibility.Hidden;
        }

        private void tilesList_SelectedItemChanged(object sender, RoutedEventArgs e)
        {
            if (!Convert.ToBoolean(test.plates.Count))
            {
                canvas.Children.Clear(); return;
            }
            if (TilesList.SelectedItem == null) return;

            if (TilesList.SelectedItem.GetType().Name == "Plate")
            {
                canvas.Children.Clear();


                Plate plate1 = (Plate)TilesList.SelectedItem;

                render(plate1);

                
            }
            if (TilesList.SelectedItem.GetType().Name == "Item")
            {
                EditFragPanel.Visibility = Visibility.Visible;
            }
            
        }

        private void render(Plate plate)
        {
            //canvas.Width = plate.width;
            //canvas.Height = plate.height;
            BinManager M = new BinManager(plate.width, plate.height, true);



            /*if (notFitList.Items.Count > 0)
            {
                notFitGB.Visibility = Visibility.Visible;
                notFitList.ItemsSource = new ObservableCollection<Item>();
                
            }*/


            M.add_items(new List<Item>(plate.items));
            M.execute();


            int fs = setCanvasScale(plate.width, plate.height); // returns suitable fontsize

            var plateRect = new Path
            {
                Data = new RectangleGeometry(new Rect(2, 2, plate.width - 2, plate.height - 2)),
                Stroke = Brushes.Black,
                StrokeThickness = 2,
                Fill = Brushes.Beige
            };
            canvas.Children.Add(plateRect);

            foreach (Item item in M.bins[0].items)
            {
                var fragRect = new Path
                {
                    Data = new RectangleGeometry(new Rect(item.x + 2, item.y + 2, item.width - 2, item.height - 2)),
                    Stroke = Brushes.Black,
                    StrokeThickness = 2,
                    Fill = new SolidColorBrush(Color.FromRgb((byte)r.Next(90, 255), (byte)r.Next(90, 255), (byte)r.Next(90, 233)))
                };

                TextBlock textblock = new TextBlock();
                textblock.FontSize = fs;
                textblock.Width = item.width;

                textblock.TextAlignment = TextAlignment.Center;

                textblock.Text = item.name;
                textblock.Margin = new Thickness(item.x, item.y + item.height / 2, 0, 0);

                TextBlock width_label = new TextBlock();
                width_label.FontSize = fs;
                width_label.Width = item.width;
                width_label.TextAlignment = TextAlignment.Center;
                width_label.Text = item.width.ToString() + "mm";
                width_label.Margin = new Thickness(item.x, item.y + 2, 0, 0);

                TextBlock height_label = new TextBlock();
                height_label.FontSize = fs;
                height_label.TextAlignment = TextAlignment.Center;
                height_label.Width = fs < 20 ? 50 : fs * 4 ;
                height_label.Text = item.height.ToString() + "mm";
                height_label.Margin = new Thickness(item.x + 2, item.y + item.height / 2 + 25, 0, 0);

                RotateTransform rotateTransform1 = new RotateTransform(-90);

                height_label.RenderTransform = rotateTransform1;

                canvas.Children.Add(fragRect);
                canvas.Children.Add(textblock);
                canvas.Children.Add(width_label);
                canvas.Children.Add(height_label);

            }
            Console.WriteLine("NOT FIT COUNT - ");
            Console.WriteLine(Convert.ToString(M.bins.Count));
            if (M.bins.Count > 1 && M.bins[1].items.Any()) { 
                Console.WriteLine(M.bins[1].items.Count.ToString());

                notFitGB.Visibility = Visibility.Visible;

                var oc = new ObservableCollection<Item>();
                M.bins[1].items.ForEach(x => oc.Add(x));
                notFitList.ItemsSource = oc;

                if (TilesList.SelectedItem.GetType().Name == "Plate")
                {
                    Plate plate1 = (Plate)TilesList.SelectedItem;

                    
                    foreach (var item in M.bins[1].items) {
                        foreach (var plItem in plate1.items)
                        {
                            if (plItem.id == item.id)
                            {   

                                plate1.removeItem(plItem);
                                break;
                            }
                        }
                    }
                }
            } else
            {
                notFitGB.Visibility = Visibility.Hidden;
                notFitList.ItemsSource = new ObservableCollection<Item>();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = System.Windows.Visibility.Visible;
        }

        private int setCanvasScale(int width, int height)
        {
            int normalFontSize = 10;
            double k = 1.0;
            if(width > 4000 || height > 4000)
            {
                k = 0.1;
                normalFontSize = 86;
            } else if(width > 2000 || height > 2000)
            {
                k = 0.3;
                normalFontSize = 60;
            }
            else if (width > 1000 || height > 1000)
            {
                k = 0.4;
                normalFontSize = 38;
            }
            else if (width > 500 || height > 500)
            {
                k = 0.7;
                normalFontSize = 19;
            }
            else { k = 1.0; }

            canvas.RenderTransform = new ScaleTransform(k, k);
            zoom = k;
            return normalFontSize;
        }

        private void updateData(object sender, RoutedEventArgs e)
        {
           /* if (notFitList.Items.Count > 0)
            {
                foreach (var notFitItem in notFitList.Items)
                {
                    
                    test.plates[idx].addItem(notFitItem as Item, 1);
                    //notFitList.Items.Remove(notFitItem as Item);
                }
            }*/
            TilesList.ItemsSource = null;
            TilesList.ItemsSource = test.plates;
            TilesList.Items.Refresh();
            var tvi = TilesList.ItemContainerGenerator.ContainerFromIndex(0) as TreeViewItem;

            if (tvi != null)
            {
                tvi.IsSelected = true;
                tvi.IsExpanded = true;
            }
        }

        private void AddPlateButton_Click(object sender, RoutedEventArgs e)
        {
            int w = 0;
            int h = 0;
            if (THeight.Text != "" && TWidth.Text != "" && int.TryParse(TWidth.Text, out w) && int.TryParse(THeight.Text, out h)) {
                // YesButton Clicked! Let's hide our InputBox and handle the input text.
                InputBox.Visibility = System.Windows.Visibility.Collapsed;
                Plate plate1 = test.addPlate(new Plate(w, h));

                // Clear InputBox.
                THeight.Text = String.Empty;
                TWidth.Text = String.Empty;
            } else
            {
                MessageBox.Show("Не всі поля заповнені або заповнені некоректно", String.Empty, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            //add to datagrid
            int w = 0;
            int h = 0;
            if (FLabel.Text != "" && FHeight.Text != "" && FWidth.Text != "" && int.TryParse(FWidth.Text, out w) && int.TryParse(FHeight.Text, out h))
            {
                var data = new Item(w, h, FLabel.Text);
                fragmentsGrid.Items.Add(data);
            }
            else
            {
                MessageBox.Show("Не всі поля заповнені або заповнені некоректно", String.Empty, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        private Guid Tid;

        private void AddFragment_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                Tid = (Guid)btn.Tag;
                InputFragBox.Visibility = System.Windows.Visibility.Visible;
            }
            
        }

        private void SaveItemsButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var tile in test.plates)
            {
                if (tile.id == Tid)
                {
                    foreach (var item in fragmentsGrid.Items.OfType<Item>())
                    {
                        tile.addItem(item, 1);
                    }
                    //TilesList.SelectedItem = null;
                    TilesList.Items.Refresh();
                    var tvi = TilesList.ItemContainerGenerator.ContainerFromItem(tile) as TreeViewItem;

                    if (tvi != null)
                    {
                        tvi.IsSelected = true;
                        tvi.IsExpanded = true;
                    }
                    break;
                }
            }

            InputFragBox.Visibility = System.Windows.Visibility.Collapsed;
            fragmentsGrid.Items.Clear();
            FHeight.Text = String.Empty;
            FWidth.Text = String.Empty;
            FLabel.Text = String.Empty;
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            // NoButton Clicked! Let's hide our InputBox.
            InputBox.Visibility = System.Windows.Visibility.Collapsed;
            InputFragBox.Visibility = System.Windows.Visibility.Collapsed;
            EditItemBox.Visibility = System.Windows.Visibility.Collapsed;

            // Clear InputBox.
            THeight.Text = String.Empty;
            TWidth.Text = String.Empty; 
        }

        private void DeletePlate_Click(object sender, RoutedEventArgs e)
        {
            if (TilesList.SelectedItem == null || TilesList.SelectedItem.GetType().Name == "Item")
            {
                MessageBox.Show("Будь-ласка, оберіть заготовку.", String.Empty, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (TilesList.SelectedItem.GetType().Name == "Plate") {
                test.removePlate(TilesList.SelectedItem as Plate);
            }
        }


        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            if(TilesList.SelectedItem == null || TilesList.SelectedItem.GetType().Name == "Plate")
            {
                MessageBox.Show("Будь-ласка, оберіть деталь.", String.Empty, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (TilesList.SelectedItem.GetType().Name == "Item")
            {
                var data = (Item)TilesList.SelectedItem;

                foreach (var (pl, index) in test.plates.Select((v, i) => (v, i)))
                {
                    foreach (var item in pl.items)
                    {
                        if (data.id == item.id)
                        {
                            if (notFitList.Items.Count > 0)
                            {
                                foreach (var notFitItem in notFitList.Items)
                                    test.plates[index].addItem(notFitItem as Item, 1);
                            }

                            test.plates[index].removeItem(TilesList.SelectedItem as Item);
                            /*put into func*/
                            
                            break;
                        }
                    }
                    
                }
            }

        }

        private void EditItemButton_Click(object sender, RoutedEventArgs e)
        {
            if (TilesList.SelectedItem == null || TilesList.SelectedItem.GetType().Name == "Plate")
            {
                MessageBox.Show("Будь-ласка, оберіть деталь.", String.Empty, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var data = (Item)TilesList.SelectedItem;
            EditItemBox.Visibility = System.Windows.Visibility.Visible;
            IWidth.Text = data.width.ToString();
            IHeight.Text = data.height.ToString();
            IName.Text = data.name.ToString();
        }


        private void UpdateItem_Click(object sender, RoutedEventArgs e)
        {
            int w = 0;
            int h = 0;
            if (IName.Text != "" && IHeight.Text != "" && IWidth.Text != "" && int.TryParse(IWidth.Text, out w) && int.TryParse(IHeight.Text, out h))
            {

                var data = (Item)TilesList.SelectedItem;
                var idx = 0;
                var tvi = new TreeViewItem();

                EditItemBox.Visibility = System.Windows.Visibility.Visible;

                foreach (var (pl, index) in test.plates.Select((v, i) => (v, i)))
                {
                    foreach (var (item, jndex) in pl.items.Select((v, i) => (v, i)))
                    {
                        if (data.id == item.id) {
                            /*-----------------------------------------------------------*/
                            idx = index;
                            /*-----------------------------------------------------------*/


                            var el = test.plates[index].items.FirstOrDefault(x => x.id == data.id);
                            el.width = Convert.ToInt32(IWidth.Text);
                            el.height = Convert.ToInt32(IHeight.Text);
                            el.name = IName.Text;
                            
                            tvi = TilesList.ItemContainerGenerator.ContainerFromItem(pl) as TreeViewItem;
                            

                            break;
                        }
                    }
                }

                
               
                //TilesList.Items.Refresh();
                if (tvi != null)
                {
                    tvi.IsSelected = true;
                    tvi.IsExpanded = true;
                }
                if (notFitList.Items.Count > 0)
                {
                    foreach (var notFitItem in notFitList.Items)
                    {

                        test.plates[idx].addItem(notFitItem as Item, 1);
                        //notFitList.Items.Remove(notFitItem as Item);
                    }
                }
                notFitList.ItemsSource = new ObservableCollection<Item>();
                //TilesList.Items.Refresh();
                if (tvi != null)
                {
                    tvi.IsSelected = true;
                    tvi.IsExpanded = true;
                }


                EditItemBox.Visibility = System.Windows.Visibility.Collapsed;
                IName.Text = String.Empty;
                IWidth.Text = String.Empty;
                IHeight.Text = String.Empty;
            } else
            {
                MessageBox.Show("Не всі поля заповнені або заповнені некоректно", String.Empty, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        private void SaveAsImgButton_Click(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvas.RenderSize.Width,
                (int)canvas.RenderSize.Height, 96d, 96d, System.Windows.Media.PixelFormats.Default);
            rtb.Render(canvas);

            var crop = new CroppedBitmap(rtb, new Int32Rect(0, 0, Convert.ToInt32(canvas.Width), Convert.ToInt32(canvas.Height)));

            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(crop));

            using (var fs = System.IO.File.OpenWrite(System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".png"))
            {
                pngEncoder.Save(fs);
            }
            MessageBox.Show("Зображення збережено", String.Empty, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void openRecent(object sender, RoutedEventArgs e)
        {
            CreateProject taskWindow = new CreateProject(this);
            taskWindow.Show();
        }

        private Double zoomMax = 5;
        private Double zoomMin = 0.1; //0.5
        private Double zoomSpeed = 0.0001;
        private Double zoom = 1;

        // Zoom on Mouse wheel
        private void Canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            zoom += zoomSpeed * e.Delta; // Ajust zooming speed (e.Delta = Mouse spin value )
            if (zoom < zoomMin) { zoom = zoomMin; } // Limit Min Scale
            if (zoom > zoomMax) { zoom = zoomMax; } // Limit Max Scale

            Point mousePos = e.GetPosition(canvas);

            if (zoom > 0.6)
            {
                canvas.RenderTransform = new ScaleTransform(zoom, zoom, mousePos.X, mousePos.Y); // transform Canvas size from mouse position
            }
            else
            {
                canvas.RenderTransform = new ScaleTransform(zoom, zoom); // transform Canvas size
            }
        }

        private void ExitApp_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void ClearDG_Click(object sender, RoutedEventArgs e)
        {
            fragmentsGrid.Items.Clear();
        }


        


        private void showInfo(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Розробили:\nКущ С.Р.\nЦибульський К.Д.\nЛуценко Д.Є.\n\nГрупа: КНТ-129СП", "Про програму", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }

}
