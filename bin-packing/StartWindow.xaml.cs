﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic;
using System.Data.SQLite;
using System.Collections.ObjectModel;

using System.IO;
using System.Drawing;
using System.Diagnostics;
using bin_packing.Properties;
using bin_packing.Views;
using System.Data;
namespace bin_packing
{

    public partial class StartWindow : Window
    {
        public ObservableCollection<Project> projects = new ObservableCollection<Project>();

        public StartWindow()
        {
            InitializeComponent();
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\Projects")) 
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"\Projects");
            }

            ProjectsViewer();
            
        }


        void ProjectsViewer()
        {
            JsonController project_files = new JsonController("./Projects");

            foreach (string s in project_files.file_viewer())
            {
                if (s != "0")
                {
                    projects.Add(new Project(project_files.json_convertor_from(s)));
                }
            }
            projectsList.ItemsSource = projects;
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Введіть назву вашого проекта", "Новий проект", " ", -1, -1);
            if (input == String.Empty) return;
            MainWindow mainWindow = new MainWindow(input);
            mainWindow.Show();
            this.Close();
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            CreateProject taskWindow = new CreateProject(this);
            taskWindow.Show();
        }

        private Guid Tid;

        private void openRecentProject(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                Tid = (Guid)btn.Tag;

                var recentP = projects.FirstOrDefault(x => x.id == Tid);

                MainWindow f = new MainWindow(recentP);
                f.Show();
                this.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow f = new MainWindow("test");
            f.Show();
        }
        
        private void showInfo(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Розробили:\nКущ С.Р.\nЦибульський К.Д.\nЛуценко Д.Є.\n\nГрупа: КНТ-129СП", "Про програму", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DB database = new DB("./DataBase.db");
            database.db_conection();
            database.post_row_plates("20x40",20,40);
        }
    }
}
