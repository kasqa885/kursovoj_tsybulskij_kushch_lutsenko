﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bin_packing
{
    class BinManager
    {
        private int bin_width;
        private int bin_height;
        private bool rotation;
        private List<Item> _items;
        private List<Guillotine> _bins = new List<Guillotine>();

        public List<Item> items
        {
            get { return _items; }
            set { _items = value; }
        }

        public List<Guillotine> bins
        {
            get { return _bins; }
            set { _bins = value; }
        }

        public BinManager(int bin_width, int bin_height, bool rotation)
        {
            this.bin_width = bin_width;
            this.bin_height = bin_height;
            items = new List<Item>();
            this.rotation = rotation;
            bins.Add(new Guillotine(bin_width, bin_height, rotation));
        }

        public void add_items(List<Item> items)
        {
            foreach (Item item in items)
            {
                this.items.Add(item);
            }

            this.items.Sort((x, y) => (y.width * y.height).CompareTo(x.width * x.height));
        }

        public bool bin_best_fit(Item item)
        {
            bool item_fits = false;
            if (item.width <= bin_width && item.height <= bin_height)
            {
                item_fits = true;
            }
            if (rotation && item.height <= bin_width && item.width <= bin_height)
            {
                item_fits = true;
            }
            if (!item_fits)
            {
                Console.WriteLine("Error! item too big for bin");
            }
            List<(((int, int), FreeRectangle, bool), Guillotine)> scores = new List<(((int, int), FreeRectangle, bool), Guillotine)>();

            foreach (Guillotine bin in bins)
            {
                ((int, int), FreeRectangle, bool) s = bin._find_best_score(item);
                if (s.Item2 != null)
                {
                    scores.Add((s, bin));
                }
            }

            if (scores.Count > 0)
            {
                (((int, int), FreeRectangle, bool), Guillotine) min = scores[0];
                foreach ((((int, int), FreeRectangle, bool), Guillotine) score in scores)
                {
                    if (score.Item1.Item1.Item1 <= min.Item1.Item1.Item1)
                    {
                        min = score;
                    }
                }
                Console.WriteLine(bins);
                return min.Item2.insert(item);
            }

            Guillotine new_bin = new Guillotine(bin_width, bin_height, rotation);
            new_bin.insert(item);
            bins.Add(new_bin);
            return true;
        }

        public void execute()
        {
            bool res;
            foreach (Item item in items)
            {
                res = bin_best_fit(item);
                Console.WriteLine(res);
            }
        }
    }
}
