﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bin_packing
{
    public class Plate: Rectangle
    {
        private Guid _id;
        public string name { get; set; }
        private ObservableCollection<Item> _items = new ObservableCollection<Item>();
        public ObservableCollection<Item> items
        {
            get { return _items; }
            set { _items = value; }
        }
        public Guid id
        {
            get { return _id; }
            set { _id = value; }
        }
        public Plate(int width, int height, string name = "default project")
        {
            this.id = Guid.NewGuid();
            this.name = width.ToString() + " x " + height.ToString();
            this.width = width;
            this.height = height;
            this.x = 0;
            this.y = 0;
        }
        public Plate() { 
        
        }
        public void addItem(Item item, int count)
        {
            for(int i = 0; i < count; i++)
            {
                _items.Add(item);
            }
        }

        public void removeItem(Item item)
        {
            _items.Remove(item);
        }

        public void updateItem(int ind, int width, int height, string name)
        {
            _items[ind].width = width;
            _items[ind].height = height;
            _items[ind].name = name;
        }
    }
}
