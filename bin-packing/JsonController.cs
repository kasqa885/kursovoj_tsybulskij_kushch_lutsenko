using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

using System.Windows;

namespace bin_packing
{
    class JsonController
    {
        private string path;
       

        public JsonController()
        {
            path = "./Projects";
           
        }
        public JsonController(string p)
        {
            path = p;
           
        }
        public string json_convertor_to(Project project)
        {
            try
            {              
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string json = serializer.Serialize(project);
                return json;
            }
            catch 
            {
                MessageBox.Show("Ups! Something was going wrong in JSon 1!");
                return "404";
            }
        }
        public Project json_convertor_from(string js_project_path)
        {/*
            try
            {*/

                using (StreamReader sr = new StreamReader(js_project_path))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    var project = serializer.Deserialize<Project>(sr.ReadToEnd());
                    return project;
                }
           /* }
            catch
            {
                Project ErrorProject = new Project("Error");
                MessageBox.Show("Ups! Something was going wrong in JSon FROM!");
                return ErrorProject;
            }*/
        }

        public void json_writer(string json,string file_name)
        {
           /* try
            {*/
                using (StreamWriter sw = new StreamWriter(path+"/"+file_name+".json", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(json);
                }
            /*}
            catch
            {
                MessageBox.Show("Ups! Something was going wrong in JSon_write!");
            }*/
        }

        public string[] file_viewer() {
            string[] files = { "0" };
            if (Directory.Exists(path))
            {
                files = Directory.GetFiles(path);
                return files;
            }
            else return files;
        }
       
    }
}
