﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bin_packing
{
    class Guillotine
    {
        private int area;
        private int free_area;
        private bool rotation;
        private List<FreeRectangle> _freerects;
        private List<Item> _items;

        public List<FreeRectangle> freerects
        {
            get { return _freerects; }
            set { _freerects = value; }
        }

        public List<Item> items
        {
            get { return _items; }
            set { _items = value; }
        }

        public Guillotine(int width, int height, bool rotation = true)
        {
            area = width * height;
            free_area = width * height;
            this.rotation = rotation;

            if (width == 0 || height == 0)
            {
                freerects = new List<FreeRectangle>();
            }
            else
            {
                freerects = new List<FreeRectangle>();
                freerects.Add(new FreeRectangle(width, height, 0, 0));
                items = new List<Item>();
                this.rotation = rotation;
            }

        }

        static bool _item_fits_rect(Item item, FreeRectangle rect, bool rotation = false)
        {
            if (!rotation && item.width <= rect.width && item.height <= rect.height)
            {
                return true;
            }

            if (rotation && item.height <= rect.width && item.width <= rect.height)
            {
                return true;
            }

            return false;
        }

        static List<FreeRectangle> _split_along_axis(FreeRectangle freeRect, Item item, bool split)
        {
            int top_x = freeRect.x;
            int top_y = freeRect.y + item.height;
            int top_h = freeRect.height - item.height;

            int right_x = freeRect.x + item.width;
            int right_y = freeRect.y;
            int right_w = freeRect.width - item.width;

            int top_w;
            int right_h;


            //true = horizontal, false = vertical
            if (split)
            {
                top_w = freeRect.width;
                right_h = item.height;
            }
            else
            {
                top_w = item.width;
                right_h = freeRect.height;
            }

            List<FreeRectangle> result = new List<FreeRectangle>();

            if (right_w > 0 && right_h > 0)
            {
                FreeRectangle right_rect = new FreeRectangle(right_w, right_h, right_x, right_y);
                result.Add(right_rect);
            }

            if (top_w > 0 && top_h > 0)
            {
                FreeRectangle top_rect = new FreeRectangle(top_w, top_h, top_x, top_y);
                result.Add(top_rect);
            }

            return result;
        }

        public List<FreeRectangle> _split_free_rect(Item item, FreeRectangle freeRect)
        {
            int w = freeRect.width - item.width;
            int h = freeRect.height - item.height;

            Console.WriteLine(item.width * h);
            Console.WriteLine(w * item.height);

            bool split = (item.width * h > w * item.height);

            return _split_along_axis(freeRect, item, true);
        }

        public void _add_item(Item item, int x, int y, bool rotate)
        {
            if (rotate)
            {
                item.rotate();
            }

            item.x = x;
            item.y = y;

            items.Add(item);
            free_area -= item.area;
        }

        public void rectangle_merge()
        {


            freerects.ForEach(delegate (FreeRectangle freerect)
            {
                List<FreeRectangle> matching_widths = new List<FreeRectangle>();
                freerects.ForEach(delegate (FreeRectangle r)
                {
                    if (r.width == freerect.width && r.x == freerect.x && r != freerect)
                    {
                        matching_widths.Add(r);
                    }
                });

                List<FreeRectangle> matching_heights = new List<FreeRectangle>();
                freerects.ForEach(delegate (FreeRectangle r)
                {
                    if (r.height == freerect.height && r.y == freerect.y && r != freerect)
                    {
                        matching_heights.Add(r);
                    }
                });

                if (matching_widths.Count > 0)
                {
                    List<FreeRectangle> widths_adjacent = new List<FreeRectangle>();
                    matching_widths.ForEach(delegate (FreeRectangle r)
                    {
                        if (r.y == freerect.y + freerect.height)
                        {
                            widths_adjacent.Add(r);
                        }
                    });

                    if (widths_adjacent.Count > 0)
                    {
                        FreeRectangle match_rect = widths_adjacent[0];
                        FreeRectangle merged_rect = new FreeRectangle(freerect.width, freerect.height + match_rect.height, freerect.x, freerect.y);
                        freerects.Remove(freerect);
                        freerects.Remove(match_rect);
                        freerects.Add(merged_rect);
                    }
                }

                if (matching_heights.Count > 0)
                {
                    List<FreeRectangle> heights_adjacent = new List<FreeRectangle>();
                    matching_heights.ForEach(delegate (FreeRectangle r)
                    {
                        if (r.x == freerect.x + freerect.width)
                        {
                            heights_adjacent.Add(r);
                        }
                    });

                    if (heights_adjacent.Count > 0)
                    {
                        FreeRectangle match_rect = heights_adjacent[0];
                        FreeRectangle merged_rect = new FreeRectangle(freerect.width + match_rect.width, freerect.height, freerect.x, freerect.y);
                        freerects.Remove(freerect);
                        freerects.Remove(match_rect);
                        freerects.Add(merged_rect);
                    }
                }
            });

        }

        static (int, int) score(FreeRectangle rect, Item item)
        {
            return (rect.Area() - item.area, Math.Min(rect.width - item.width, rect.height - item.height));
        }

        public ((int, int), FreeRectangle, bool) _find_best_score(Item item)
        {
            List<((int, int), FreeRectangle, bool)> rects = new List<((int, int), FreeRectangle, bool)>();
            freerects.ForEach(delegate (FreeRectangle rect)
            {
                if (_item_fits_rect(item, rect))
                {
                    rects.Add((score(rect, item), rect, false));
                }

                if (rotation && _item_fits_rect(item, rect, true))
                {
                    rects.Add((score(rect, item), rect, true));
                }
            });
            try
            {
                ((int, int), FreeRectangle, bool) lowest = rects[0];
                foreach (((int, int), FreeRectangle, bool) rect in rects)
                {
                    if (rect.Item1.Item1 <= lowest.Item1.Item1)
                    {
                        lowest = rect;
                    }
                }

                return lowest;
            }
            catch
            {
                return (((0, 0), null, false));
            }
        }

        public bool insert(Item item)
        {
            ((int, int), FreeRectangle, bool) data = this._find_best_score(item);

            (int, int) size = data.Item1;
            FreeRectangle best_rect = data.Item2;
            bool rotated = data.Item3;

            if (best_rect != null)
            {
                _add_item(item, best_rect.x, best_rect.y, rotated);
                freerects.Remove(best_rect);
                List<FreeRectangle> splits = _split_free_rect(item, best_rect);

                foreach (FreeRectangle rect in splits)
                {
                    freerects.Add(rect);
                }

                rectangle_merge();

                return true;
            }
            return false;
        }
    }
}
