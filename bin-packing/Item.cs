﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bin_packing
{
    public class Item : Rectangle
    {
        private int _area;
        private bool _rotated;
        private Guid _id;
        private string _name;

        public int area
        {
            get { return _area; }
            set { _area = value; }
        }

        public bool rotated
        {
            get { return _rotated; }
            set { _rotated = value; }
        }

        public Guid id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Item(int width, int height, string name)
        {
            this.width = width;
            this.height = height;
            this.name = name;
            x = 0;
            y = 0;
            area = width * height;
            rotated = false;
            id = Guid.NewGuid();
        }
        public Item() { 
        }
        public void rotate()
        {
            int buf = height;
            height = width;
            width = buf;
            rotated = (rotated == true) ? false : true;
        }
    }
}
